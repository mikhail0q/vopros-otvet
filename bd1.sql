-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1
-- Время создания: Май 19 2018 г., 03:27
-- Версия сервера: 5.5.25
-- Версия PHP: 5.3.13

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `bd1`
--

-- --------------------------------------------------------

--
-- Структура таблицы `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Categor` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `categories`
--

INSERT INTO `categories` (`id`, `Categor`) VALUES
(1, 'Спорт'),
(2, 'Математика');

-- --------------------------------------------------------

--
-- Структура таблицы `otvet`
--

CREATE TABLE IF NOT EXISTS `otvet` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `otvet` text NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_vopros` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Дамп данных таблицы `otvet`
--

INSERT INTO `otvet` (`id`, `otvet`, `id_user`, `id_vopros`) VALUES
(1, 'ответ на "Главный вопрос жизни, вселенной и всего такого" 42\n', 10, 4),
(11, 'А вот и ЛУЧШИЙ ответ!', 10, 5),
(12, '1', 10, 5);

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `login` varchar(191) DEFAULT NULL,
  `email` varchar(191) DEFAULT NULL,
  `password` varchar(191) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `login`, `email`, `password`) VALUES
(10, '123', '122@12', '202cb962ac59075b964b07152d234b70');

-- --------------------------------------------------------

--
-- Структура таблицы `vopros`
--

CREATE TABLE IF NOT EXISTS `vopros` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vopros` text NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_categories` int(11) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `opisanie` varchar(191) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Дамп данных таблицы `vopros`
--

INSERT INTO `vopros` (`id`, `vopros`, `id_user`, `id_categories`, `time`, `opisanie`) VALUES
(1, '123', 10, 1, '2018-05-13 10:37:30', NULL),
(2, '123', 10, 2, '2018-05-13 10:55:07', NULL),
(4, 'Главный вопрос жизни, вселенной и всего такого', 0, 2, '2018-05-16 15:06:40', NULL),
(5, 'Лучший вопрос', 10, 2, '2018-05-18 18:40:57', 'Лучшее описание вопроса');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
