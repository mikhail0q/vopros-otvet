<?php
require "/reg/db.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Vopros</title>
    <link rel="shortcut icon" type="image/x-icon" href="images/ikonsait.png">
    <link rel="stylesheet" href="style.css" media="all">
</head>
<body>
 <div class="header" >
    <div class="mid"> 
    <header>
       <div class="topmenu">
          <div class="registr">
           <aside>
                <?php if (isset($_SESSION['logged_user']) ) : ?> 
<div style="color: blue">Privet, <?php echo $_SESSION['logged_user']->login ?>!
<a href="logout.php">Выйти</a></div>
<?php else : ?>
<a href="login.php">Войти</a>
<a href="registr.php">Регистрация</a>
<?php endif; ?>
           </aside>
           </div>
       </div>
        <img src="images/logoo.png" alt="Логотип сайта" title="Логотип сайта">
        <div class="afisha">
        <img src="images/reklama.png" alt="реклама сайта" title="реклама сайта">
        </div>
    </header>
   
    </div>
   </div> 
   <div class="menu">
       <div class="mid">
          <nav>
              <ul>
                  <li><a href="index.php">Главная</a></li>
                  <li><a href="vopros.php">Вопросы</a></li>
                  <li><a href="spvoprosov.php">Задать вопрос</a></li>
              </ul>
          </nav>
       </div>
   </div>  
   <div class="content">
       <div class="mid">
       <div class="fon">
           <div class="block">
             <div class="anons">
              <section>
                     <img src="images/ikonsait.png" alt="Анимация в CSS3">
                     <h3>
<?php
 R::freeze( TRUE );
$data = $_POST;
if (isset($data['do_signup']) ) 
{

  $errors = array();
  if ( trim($data['login']) == '') 
  {
    $errors[] = 'Введите Логин!';
  }

  if (trim($data['email']) == '') 
  {
    $errors[] = 'Введите email!';
  }

  if ( $data['password'] == '') 
  {
    $errors[] = 'Введите пароль!';
  }

  if ( $data['password_2'] != $data['password']) 
  {
    $errors[] = 'Введенные вами пароли не совпадают!';
  }

    if ( R::count( 'users', "login = ?", array($data['login'])) > 0 ) 
  {
    $errors[] = 'Такой Логин уже существует!';
  }

  if ( R::count( 'users', "email = ?", array($data['email'])) > 0 ) 
  {
    $errors[] = 'Такой email уже зарегестрирован!';
  }

  if (empty($errors) ) 
  {
    $user = R::dispense('users');
    $user->login = $data['login'];
    $user->email = $data['email'];
    $user->password = md5($data['password']);
    R::store($user);
     echo '<div style="color: blue;">Вы успешно зарегестрированы!</div><hr>';
  } else
  {
 echo '<div style="color: red;">'.array_shift($errors). '</div><hr>';
  }
}

?>
<form action="registr.php" method="POST">
<p>
  <p><strong>Введите логин</strong>:</p>
<input type="text" name="login" value="<?php echo @$data['login'];?>">
</p>

<p>
  <p><strong>Введите email</strong>:</p>
<input type="email" name="email" value="<?php echo @$data['email'];?>">
</p>

<p>
  <p><strong>Введите пароль</strong>:</p>
<input type="password" name="password" >
</p>

<p>
  <p><strong>Введите ваш пароль еще раз</strong>:</p>
<input type="password" name="password_2">
</p>

<p>
  <button type="submit" name="do_signup">Регистрация</button>
</p>
</form>
</h3>
                 </section>
             </div>    
             </div>  
           </div>
       </div>
   </div>
   <div class="footer">
       <div class="mid">
         <form name="f1" method="post" action="search.php">
<input type="search" name="search_q"/></br>
<select class="form-control" name="a"> 
<option value="0" >Выберите из списка</option> 
<?php
$sql = mysqli_query($connection,"SELECT * FROM `categories` ORDER BY `Categor`"); 

$array = array(); 

$i = 0; 

while ( $row = mysqli_fetch_assoc( $sql ) ) { 

$array[ $i ][ 'id' ] = $row[ 'id' ]; 
$array[ $i ][ 'name' ] = $row[ 'Categor' ]; 
$i++; 

} 
foreach ($array as $array) { 
print '<option value= "' . $array[ 'id' ] . '" 0>' . $array[ 'name' ] . '</option>'; 
} 

?> 

</select></br></br>
<input type="submit" value="Поиск"/></br>
</form>
       </div>
   </div>
</body>
</html>